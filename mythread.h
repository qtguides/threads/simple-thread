#ifndef MYTHREAD_H
#define MYTHREAD_H

#include <QThread>

class MyThread : public QThread
{
    Q_OBJECT
public:
    explicit MyThread(QString nombre, QObject *parent = nullptr);

    void run() override;

private:
    QString nombre;

signals:

public slots:

};

#endif // MYTHREAD_H
