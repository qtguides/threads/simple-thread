#include "mainwindow.h"
#include <QApplication>

#include "mythread.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    MyThread thread1("A"), thread2("B"), thread3("C");

    thread1.start();
    thread2.start();
    thread3.start();

    thread3.wait();
    thread2.wait();
    thread1.wait();

    return a.exec();
}
