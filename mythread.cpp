#include "mythread.h"
#include <QDebug>

MyThread::MyThread(QString nombre, QObject *parent) :
    QThread(parent),
    nombre(nombre)
{
}

void MyThread::run()
{
    for(int i = 0; i <= 100; i++)
    {
        qDebug() << this->nombre << " " << i;
    }
}
